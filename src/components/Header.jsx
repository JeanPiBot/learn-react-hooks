import React, { useState, useContext } from 'react';
import ThemeContext from '../context/ThemeContext';

const Header = () => {
    const [darkMode, setDarkMode] = useState(false);
    const color = useContext(ThemeContext);

    const handleClicked = () => {
        setDarkMode(!darkMode);
    }

    return (
        <div className="header">
            <h1 style={{ color }}>StoreHooks</h1>
            <button type="button" onClick={handleClicked}>{darkMode ? ' Dark Mode' : 'Light Mode'}</button>

            {/* Otra Forma de escribir el handleClick sin escribir la función */}
            {/* <button type="button" onClick={() => setDarkMode(!darkMode)}>{darkMode ? ' Dark Mode2' : 'Light Mode2'}</button> */}
        </div>
    );
}


export default Header;